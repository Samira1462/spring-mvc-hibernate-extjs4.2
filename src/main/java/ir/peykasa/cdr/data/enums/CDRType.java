package ir.peykasa.cdr.data.enums;

/**
 * Created by Samira on 8/7/2017.
 */
public enum CDRType {
    ATTACH(3),
    DETACH(4),
    REGISTER(5),
    ENTER(6),
    EXIT(7)
    ;
    private int code;

    CDRType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static CDRType findByType(int type){
        switch (type) {
            case 3 : return ATTACH;
            case 4 : return DETACH;
            case 5 : return REGISTER;
            case 6 : return ENTER;
            case 7 : return EXIT;
        }
        return null;
    }
}
