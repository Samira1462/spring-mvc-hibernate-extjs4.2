package ir.peykasa.cdr.data.ob;

import java.io.Serializable;

/**
 * Created by Samira on 8/10/2017.
 */
public class CDRObject implements Serializable {
    private String sortField;
    private String sortOrder;

    public CDRObject() {

    }

    public CDRObject(String sortOrder, String sortField) {
        this.sortOrder = sortOrder;
        this.sortField = sortField;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
