package ir.peykasa.cdr.data.tx;

import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;

/**
 * Created by Samira on 8/11/2017.
 */
public class CDRTransactionManager extends org.springframework.orm.hibernate5.HibernateTransactionManager {

    @Override
    protected void doBegin(Object transaction, TransactionDefinition definition) {
        super.doBegin(transaction, definition);
    }

    @Override
    protected void doCommit(DefaultTransactionStatus status) {
        super.doCommit(status);
    }

    @Override
    protected void doRollback(DefaultTransactionStatus status) {
        super.doRollback(status);
    }
}
