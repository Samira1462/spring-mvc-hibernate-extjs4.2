package ir.peykasa.cdr.data.listmodel;

import java.io.Serializable;

/**
 * Created by Samira on 8/11/2017.
 */
public class CDRReportTypeListModel implements Serializable {

    private String cdrType;
    private Integer count;

    public String getCdrType() {
        return cdrType;
    }

    public void setCdrType(String cdrType) {
        this.cdrType = cdrType;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
