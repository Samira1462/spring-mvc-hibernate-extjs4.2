package ir.peykasa.cdr.data.listmodel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Samira on 8/10/2017.
 */
public class BaseListModel<T> implements Serializable {
    private Integer count;
    private List<T> items;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
