package ir.peykasa.cdr.data.entity;

import ir.peykasa.cdr.data.enums.CDRType;

import javax.persistence.*;

/**
 * Created by Samira on 8/4/2017.
 */
@Entity
@Table(name = "tbl_cdr")
@SequenceGenerator(name = "SEQ_GEN", sequenceName = "seq_cdr", initialValue = 1, allocationSize = 1)
public class CDRObject {
    private Long id;
    private String time;
    private String sender;
    private String receiver;
    private CDRType type;
    private String message;


    @Id
    @Column(name = "id", length = 20 , nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cdr_time", length = 20 , nullable = false)
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Basic
    @Column(name = "sender", length = 15, nullable = false)
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
    @Basic
    @Column(name = "receiver", length = 15, nullable = false)
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "cdr_type", columnDefinition = "VARCHAR2(20)")
    public CDRType getType() {
        return type;
    }

    public void setType(CDRType type) {
        this.type = type;
    }


    @Basic
    @Column(name = "message",length = 255, nullable = false)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
