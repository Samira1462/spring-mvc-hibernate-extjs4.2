package ir.peykasa.cdr.data.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * This class is parent for all RailWayGraph entity's.
 */
@MappedSuperclass
public abstract class ParentOfModels<PK extends Long> implements Serializable, Cloneable {

    @Basic
    @Column(updatable = true, nullable = true, name = "deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(updatable = false, length = 50, nullable = true, name = "create_datetime")
    public Timestamp getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Timestamp timestamp) {
        createDateTime = timestamp;
    }


    @Basic
    @Column(updatable = false, length = 100, nullable = true, name = "create_uid")
    public String getCreateUID() {
        return createUID;
    }

    public void setCreateUID(String string) {
        createUID = string;
    }

    @Basic
    @Column(updatable = true, nullable = true, name = "update_datetime")
    public Timestamp getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Timestamp timestamp) {
        updateDateTime = timestamp;
    }

    @Basic
    @Column(updatable = true, nullable = true, name = "update_uid")
    public String getUpdateUID() {
        return updateUID;
    }

    public void setUpdateUID(String string) {
        updateUID = string;
    }

    @Version
    @Column(updatable = true, nullable = true, name = "version")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long long1) {
        version = long1;
    }


    @Transient
    public boolean isNew() {
        boolean result = (getId() == null);
        if ((getId() != null)) {
            result |= getId().longValue() <= 0;
        }
        return result;
    }

    @Override
    public Object clone() {
        try {
            ParentOfModels clone = (ParentOfModels) super.clone();
            clone.setId(null);
            clone.setCreateDateTime(null);
            clone.setCreateUID(null);
            clone.setUpdateDateTime(null);
            clone.setUpdateUID(null);
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    protected ParentOfModels() {
    }

    protected ParentOfModels(PK pk) {
        setId(pk);
    }

    @Transient
    public abstract PK getId();

    public abstract void setId(PK pk);

    //record info fields
    protected Long version;
    protected Timestamp updateDateTime;
    protected String updateUID;
    protected Timestamp createDateTime;
    protected String createUID;
    protected Boolean deleted = false;

}
