package ir.peykasa.cdr.data.dao;

import ir.peykasa.cdr.data.entity.CDRObject;
import ir.peykasa.cdr.data.so.BaseSearchObject;
import ir.peykasa.cdr.data.so.CDRReportSO;

import java.util.List;

/**
 * Created by Samira on 8/11/2017.
 */
public interface CDRReportDao {
    List<CDRObject> getCDRObjectReport(BaseSearchObject<CDRReportSO> so);

    Integer getCountAllCDRObject(BaseSearchObject<CDRReportSO> so);

    List<String> getCDRTypeObjectReport(BaseSearchObject<CDRReportSO> so);
}
