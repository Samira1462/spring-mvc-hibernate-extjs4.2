package ir.peykasa.cdr.data.dao.impl;

import ir.peykasa.cdr.commons.hibernate.HibernateFactory;
import ir.peykasa.cdr.commons.utils.DbStringUtil;
import ir.peykasa.cdr.data.dao.CDRReportDao;
import ir.peykasa.cdr.data.entity.CDRObject;
import ir.peykasa.cdr.data.enums.CDRType;
import ir.peykasa.cdr.data.so.BaseSearchObject;
import ir.peykasa.cdr.data.so.CDRReportSO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Samira on 8/11/2017.
 */
@Repository("cdrReportDao")
public class CDRReportDaoImpl extends HibernateFactory implements CDRReportDao {

     @Override
    public List<ir.peykasa.cdr.data.entity.CDRObject> getCDRObjectReport(BaseSearchObject<CDRReportSO> so) {
        Integer from = null;
        Integer limit = null;
        CDRReportSO ob = so.getOb();
        if (so != null) {
            from = so.getFrom();
            limit = so.getLimit();
        }
        String tableName = "tbl_cdr cdr";
        String column = returnColumn(so.getSo());
        String whereClause = returnWhereClause(so.getSo());
        String orderByClause = returnOrderByClause(so.getOb());
        StringBuilder queryString = createQueryString(tableName, column, whereClause, orderByClause);
        return createQuery(queryString, so.getSo(), orderByClause, limit, from);
    }

    @Override
    public Integer getCountAllCDRObject(BaseSearchObject<CDRReportSO> so) {
        Integer from = null;
        Integer limit = null;
        String tableName = "tbl_cdr cdr";
        String column = "count(*)";
        String whereClause = returnWhereClause(so.getSo());
        String orderByClause = returnOrderByClause(so.getOb());
        StringBuilder queryString = createQueryString(tableName, column, whereClause, orderByClause);
        return createQuery(queryString, so.getSo(), orderByClause, limit, from).size();
    }

    @Override
    public List<String> getCDRTypeObjectReport(BaseSearchObject<CDRReportSO> so) {
        Integer from = null;
        Integer limit = null;
        if (so != null) {
            from = so.getFrom();
            limit = so.getLimit();
        }
        String tableName = "tbl_cdr cdr";
        String column = returnColumn(so.getSo());
        String whereClause = returnWhereClause(so.getSo());
        String orderByClause = returnOrderByClause(so.getOb());
        StringBuilder queryString = createQueryString(tableName, column, whereClause, orderByClause);
        return createCDRTypeObjectReportQuery(queryString, so.getSo(), orderByClause, limit, from);
    }

    private List<String> createCDRTypeObjectReportQuery(StringBuilder queryString, CDRReportSO searchObject, String orderByClause, Integer maxResults, Integer firstResult) {
        Session session = getCurrentSession();
        final Query query;
        try {
            List list;
            if ((maxResults != null) && (firstResult != null) && maxResults > 0 && firstResult > -1) {
                query = session.createSQLQuery(queryString.toString());
                query.setFirstResult(firstResult);
                query.setMaxResults(maxResults);
                bindParameter(searchObject, query);
                return query.list();
            } else {
                query = session.createSQLQuery(queryString.toString());
                bindParameter(searchObject, query);
                list = query.list();
            }
            return list;
        } finally {
            closeSession(session);
        }
    }

    private void setupSearchObject(CDRReportSO so) {
        if (so == null) {
            return;
        }
        String message = so.getMessage();
        if (message != null) {
            so.setMessage("%" + DbStringUtil.replaceSymbol(message).toUpperCase().replace(" ", "%") + "%");
        } else {
            so.setMessage(null);
        }
    }

//    (DbStringUtil.replaceSymbol(searchObject.getMessage()).toUpperCase().replace(" ", "%") )

    private List<ir.peykasa.cdr.data.entity.CDRObject> createQuery(StringBuilder queryString, CDRReportSO searchObject, String ob, Integer maxResults, Integer firstResult) {
        Session session = getCurrentSession();
        final Query query;
        try {
            List list;
            List<CDRObject> result = new ArrayList<>();
            if ((maxResults != null) && (firstResult != null) && maxResults > 0 && firstResult > -1) {
                query = session.createSQLQuery(queryString.toString());
                query.setFirstResult(firstResult);
                query.setMaxResults(maxResults);
                bindParameter(searchObject, query);
                list = query.list();
                for (Object obj : list) {
                    result.add(convert((Object[]) obj));
                }
                return result;
            } else {
                query = session.createSQLQuery(queryString.toString());
                bindParameter(searchObject, query);
                list = query.list();
                for (Object o : list) {
                    result.add(convert((Object[]) o));
                }
            }
            return result;
        } finally {
            closeSession(session);
        }
    }

    private CDRObject convert(Object[] obj) {
        CDRObject cdrObject = new CDRObject();
        cdrObject.setId(Long.parseLong(obj[0].toString()));
        cdrObject.setTime(obj[1].toString());
        cdrObject.setSender(obj[2].toString());
        cdrObject.setReceiver(obj[3].toString());
        cdrObject.setType(CDRType.valueOf((obj[4]).toString()));
        cdrObject.setMessage((obj[5].toString()));
        return cdrObject;
    }

    private void bindParameter(CDRReportSO searchObj, Query query) {

        if (searchObj != null && searchObj.getMessage() != null) {
            String message = searchObj.getMessage();
            message = "%" + message + "%";
            query.setParameter("searchKey", message);
        }

        if (searchObj != null && searchObj.getCdrType() != null) {

            List<String> stringList = Arrays.asList(searchObj.getCdrType().split(","));
            for (int i = 0; i < stringList.size(); i++) {
                if (stringList.get(i) != null) {
                    String searchkey = "searchkey" + i;
                    query.setParameter(searchkey, stringList.get(i));
                }
            }
        }

        if (searchObj != null && searchObj.getFromDate() != null && searchObj.getToDate() != null) {
            query.setParameter("searchKey", searchObj.getFromDate());
            query.setParameter("searchKey2", searchObj.getToDate());

        }
    }

    private StringBuilder createQueryString(String tableName, String column, String whereCluse, String OrderBy) {
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT ");
        builder.append(column);
        builder.append(" From ");
        builder.append(tableName);
        builder.append(" where ");
        builder.append(whereCluse);
        if (OrderBy != null) {
            builder.append(" ORDER BY ");
            builder.append(OrderBy);
        }

//        builder.append("SELECT cdr.message From tbl_cdr cdr where upper(cdr.message) :searchObject");
        return builder;
    }


    private String returnColumn(CDRReportSO searchObj) {
        String column = null;
        if (searchObj != null && searchObj.getMessage() != null) {
            column = "*";
        }

        if (searchObj != null && searchObj.getCdrType() != null) {
            column = "cdr.cdr_type";
        }

        if (searchObj != null && searchObj.getFromDate() != null && searchObj.getToDate() != null) {
            column = "*";
        }
        return column;
    }

    private String returnWhereClause(CDRReportSO searchObj) {

        String whereClause = null;
        if (searchObj != null && searchObj.getMessage() != null) {
            whereClause = "cdr.message LIKE :searchKey";
        }

        if (searchObj != null && searchObj.getCdrType() != null) {
            whereClause = "";
            List<String> stringList = Arrays.asList(searchObj.getCdrType().split(","));
            for (int i = 0; i < stringList.size(); i++) {
                whereClause += "cdr.cdr_type LIKE :searchkey" + i + " || ";
            }
            whereClause = whereClause.substring(0, whereClause.length() - 3);
        }

        if (searchObj != null && searchObj.getFromDate() != null && searchObj.getToDate() != null) {
            whereClause = "cdr_Time BETWEEN :searchKey AND :searchKey2";
        }

        return whereClause;
    }

    private String returnOrderByClause(CDRReportSO orderBy) {

        String orderByClause = null;

        if (orderBy != null && orderBy.getMessage() != null) {
            orderByClause = "ORDER BY 'cdr.message'";
        }

        if (orderBy != null && orderBy.getCdrType() != null) {
            orderByClause = "ORDER BY 'cdr.cdr_type'";
        }

        if (orderBy != null && orderBy.getFromDate() != null && orderBy.getToDate() != null) {
            orderByClause = "ORDER BY 'cdr.date'";
        }
        return orderByClause;
    }


}
