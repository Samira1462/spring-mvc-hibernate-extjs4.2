package ir.peykasa.cdr.data.so;

import java.io.Serializable;

/**
 * Created by Samira on 8/10/2017.
 */
public class BaseSearchObject<T> implements Serializable {
    private Integer from;
    private Integer limit;
    private T so;
    private T ob;


    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTo(){
        if((from != null) && (limit != null)){
            return from + limit;
        }
        return null;
    }

    public Integer getPage(){
        if((from == null) || (limit == null)){
            return null;
        }
        return ((from / limit));
    }

    public T getSo() {
        return so;
    }

    public void setSo(T so) {
        this.so = so;
    }

    public T getOb() {
        return ob;
    }

    public void setOb(T ob) {
        this.ob = ob;
    }

    public boolean getDeleted(){
        return false;
    }
    public boolean getCanceled(){
        return false;
    }
}
