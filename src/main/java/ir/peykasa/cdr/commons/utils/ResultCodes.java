package ir.peykasa.cdr.commons.utils;

/**
 * Created by Samira on 8/1/2017.
 */
public class ResultCodes {

    public static final String SUCCESS_RESULT_CODE = "200";
    public static final String UNKNOWN_SERVER_ERROR = "500";
    public static final String NOT_VALID_MAXIMUM_NUMBER_OF_LINES_EXCEPTION = "101";
    public static final String NOT_VALID_NUMBER_FOR_CDR_SYSTEM = "102";
}
