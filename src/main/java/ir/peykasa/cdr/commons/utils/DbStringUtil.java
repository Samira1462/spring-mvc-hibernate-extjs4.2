package ir.peykasa.cdr.commons.utils;

/**
 * Created by Samira on 8/11/2017.
 */
public class DbStringUtil {

    public static String replaceSymbol(String str) {
        String replace = null;
        if (str != null) {
            replace = str.trim().replace("_", "\\_");
        }
        return replace;
    }
}
