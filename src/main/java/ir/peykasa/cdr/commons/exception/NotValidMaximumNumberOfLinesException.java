package ir.peykasa.cdr.commons.exception;


import ir.peykasa.cdr.commons.utils.ResultCodes;

/**
 * Created by Radmaneshfar on 8/1/2017.
 */
public class NotValidMaximumNumberOfLinesException extends CDRException {
    public NotValidMaximumNumberOfLinesException() {
        putErrorArg("NotValidMaximumNumberOfLinesException","");
    }

    @Override
    public String getErrorCode() {
        return ResultCodes.NOT_VALID_MAXIMUM_NUMBER_OF_LINES_EXCEPTION;
    }
}
