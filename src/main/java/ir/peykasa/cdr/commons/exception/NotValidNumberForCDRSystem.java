package ir.peykasa.cdr.commons.exception;


import ir.peykasa.cdr.commons.utils.ResultCodes;

/**
 * Created by Samira on 8/1/2017.
 */
public class NotValidNumberForCDRSystem extends CDRException {
    public NotValidNumberForCDRSystem() {
        putErrorArg("NotValidNumberForCDRSystem","");
    }

    @Override
    public String getErrorCode() {
        return ResultCodes.NOT_VALID_NUMBER_FOR_CDR_SYSTEM;
    }
}
