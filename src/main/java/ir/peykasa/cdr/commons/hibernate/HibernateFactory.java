package ir.peykasa.cdr.commons.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Samira on 8/12/2017.
 */
public abstract class HibernateFactory {

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getCurrentSession() {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            if (currentSession.isOpen()) {
                return currentSession;
            }
        } catch (HibernateException ignored) {
        }
        return sessionFactory.openSession();
    }


    public void closeSession(Session session) {
        Transaction transaction = session.getTransaction();
        if (transaction == null || !transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
            session.close();
        }
    }


}
