package ir.peykasa.cdr.service.impl;

import ir.peykasa.cdr.data.dao.CDRReportDao;
import ir.peykasa.cdr.data.entity.CDRObject;
import ir.peykasa.cdr.data.enums.CDRType;
import ir.peykasa.cdr.data.listmodel.BaseListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportTypeListModel;
import ir.peykasa.cdr.data.so.BaseSearchObject;
import ir.peykasa.cdr.data.so.CDRReportSO;
import ir.peykasa.cdr.service.CDRReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Samira on 8/11/2017.
 */
@Service("cdrReportService")
public class CDRReportServiceImpl implements CDRReportService {

    @Autowired
    private CDRReportDao cdrReportDao;

    @Override
    public BaseListModel<CDRReportListModel> getCDRObjectReport(BaseSearchObject<CDRReportSO> so) {
        BaseListModel<CDRReportListModel> baseListModel = new BaseListModel<>();

        final List<CDRObject> cdrObjectReport = cdrReportDao.getCDRObjectReport(so);
        List<CDRReportListModel> list = new ArrayList<>();

        for (CDRObject cdrObject : cdrObjectReport) {
            CDRReportListModel reportListModel = new CDRReportListModel();
            reportListModel.setMessage(cdrObject.getMessage());
            reportListModel.setId(cdrObject.getId());
            reportListModel.setReceiver(cdrObject.getReceiver());
            reportListModel.setSender(cdrObject.getSender());
            reportListModel.setTime(cdrObject.getTime());
            CDRType type = cdrObject.getType();
            reportListModel.setType(type.name());
            list.add(reportListModel);
        }

        baseListModel.setItems(list);
        baseListModel.setCount(list.size());
        return baseListModel;
    }

    @Override
    public BaseListModel<CDRReportTypeListModel> getReportByType(BaseSearchObject<CDRReportSO> so) {
        BaseListModel<CDRReportTypeListModel> baseListModel = new BaseListModel<>();
        List<CDRReportTypeListModel> typeListModels = new ArrayList<>();
        List<String> allCdrType = cdrReportDao.getCDRTypeObjectReport(so);
        Set<String> unique = new HashSet<>(allCdrType);
        for (String key : unique) {
            CDRReportTypeListModel cdrReportTypeListModel = new CDRReportTypeListModel();
            cdrReportTypeListModel.setCount(Collections.frequency(allCdrType, key));
            cdrReportTypeListModel.setCdrType(key);
            typeListModels.add(cdrReportTypeListModel);
        }
        baseListModel.setItems(typeListModels);
        baseListModel.setCount(typeListModels.size());
        return baseListModel;
    }
}
