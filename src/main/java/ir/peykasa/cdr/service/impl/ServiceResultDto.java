package ir.peykasa.cdr.service.impl;

import ir.peykasa.cdr.commons.exception.CDRException;
import ir.peykasa.cdr.commons.utils.ResultCodes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Samira on 8/11/2017.
 */
public class ServiceResultDto<T> implements Serializable {
    private String resultCode;
    private Map<String, String> error;
    private T value;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public Map<String, String> getError() {
        return error;
    }

    public void setError(Map<String, String> error) {
        this.error = error;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setPublicError(Exception e) {
        if (this.error == null) {
            this.error = new HashMap<>();
        }
        e.printStackTrace();
        error.put("exception_class", e.getClass().getName());
        setValue(null);
        setResultCode(ResultCodes.UNKNOWN_SERVER_ERROR);
    }

    public void setErrorInfo(CDRException e) {
        if (this.error == null) {
            this.error = new HashMap<>();
        }
        setError(e.getArgs());
        setResultCode(e.getErrorCode());
        setValue(null);
    }
}

