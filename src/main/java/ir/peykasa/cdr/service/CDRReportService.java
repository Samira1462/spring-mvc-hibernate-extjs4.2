package ir.peykasa.cdr.service;

import ir.peykasa.cdr.data.listmodel.BaseListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportTypeListModel;
import ir.peykasa.cdr.data.so.BaseSearchObject;
import ir.peykasa.cdr.data.so.CDRReportSO;

/**
 * Created by Samira on 8/11/2017.
 */
public interface CDRReportService {
    BaseListModel<CDRReportListModel> getCDRObjectReport(BaseSearchObject<CDRReportSO> so);

    BaseListModel<CDRReportTypeListModel> getReportByType(BaseSearchObject<CDRReportSO> so);
}
