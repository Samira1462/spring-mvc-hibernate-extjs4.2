package ir.peykasa.cdr.controller;

import ir.peykasa.cdr.commons.exception.CDRException;
import ir.peykasa.cdr.commons.utils.ResultCodes;
import ir.peykasa.cdr.data.listmodel.BaseListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportListModel;
import ir.peykasa.cdr.data.listmodel.CDRReportTypeListModel;
import ir.peykasa.cdr.data.so.BaseSearchObject;
import ir.peykasa.cdr.data.so.CDRReportSO;
import ir.peykasa.cdr.service.CDRReportService;
import ir.peykasa.cdr.service.impl.ServiceResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Samira on 8/11/2017.
 */
@RestController
@RequestMapping(value = "/cdr/report")
public class CDRReportRestController {

    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";

    private static final Logger logger = LoggerFactory.getLogger(CDRReportRestController.class);

    @Autowired(required = true)
    private CDRReportService cdrReportService;


    @RequestMapping(value = "/list", method = RequestMethod.POST, consumes = APPLICATION_JSON_CHARSET_UTF_8)
    public
    @ResponseBody
    ServiceResultDto<BaseListModel<CDRReportListModel>> getReport(@RequestBody BaseSearchObject<CDRReportSO> so) {
        ServiceResultDto<BaseListModel<CDRReportListModel>> result = new ServiceResultDto<>();
        try {
            logger.info("Start to get message and date report");
            BaseListModel<CDRReportListModel> cdrReportList = cdrReportService.getCDRObjectReport(so);
            result.setValue(cdrReportList);
            result.setResultCode(ResultCodes.SUCCESS_RESULT_CODE);
        } catch (CDRException e) {
            result.setErrorInfo(e);
        } catch (Exception e) {
            e.printStackTrace();
            result.setPublicError(e);
        }
        return result;
    }

    @RequestMapping(value = "type/list", method = RequestMethod.POST, consumes = APPLICATION_JSON_CHARSET_UTF_8)
    public
    @ResponseBody
    ServiceResultDto<BaseListModel<CDRReportTypeListModel>> getReportByType(@RequestBody BaseSearchObject<CDRReportSO> so) {
        ServiceResultDto<BaseListModel<CDRReportTypeListModel>> result = new ServiceResultDto<>();
        try {
            logger.info("Start to get cdr type");
            BaseListModel<CDRReportTypeListModel> cdrReportByType =  cdrReportService.getReportByType(so);
            result.setValue(cdrReportByType);
            result.setResultCode(ResultCodes.SUCCESS_RESULT_CODE);
        } catch (CDRException e) {
            result.setErrorInfo(e);
        } catch (Exception e) {
            result.setPublicError(e);
        }
        return result;
    }
}
