package ir.peykasa.cdr.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Samira on 8/17/2017.
 */
@Controller
public class BaseController {

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String lunchStartPageApp(){
        return "index";
    }
}
