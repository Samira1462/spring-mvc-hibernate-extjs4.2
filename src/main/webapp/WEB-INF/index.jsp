<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>cdrReport</title>

    <link href="<c:url value="../resources/bootstrap.css" />" rel="stylesheet">
    <script src="<c:url value="../resources/ext/ext-all-rtl-dev.js" />"></script>
    <script src="<c:url value="../resources/bootstrap.js" />"></script>
    <script src="<c:url value="../resources/app.js" />"></script>

</head>
<body></body>
</html>

