/**
 * Created by Samira on 8/12/17.
 */
Ext.define('cdrReport.controller.CdrController', {
    extend: 'Ext.app.Controller',

    stores: ['CdrStore'],

    models: ['CdrModel'],

    views: ['Cdr'],

    refs: [{
        ref: 'cdr',
        selector: 'cdr'
    }],

    id: 'cdrcontroller',

    init: function () {
        this.control({
            'cdr button[itemId=search]': {
                click: this.doCdrReportJob
            }
        });
    },

    doCdrReportJob: function () {
        "use strict";
        debugger;
        var form = this.down('form').getForm();
        if (form.isValid()) {
            form.submit({
                method: 'POST',
                url: 'http://127.0.0.1:8080/cdr/report/list',
                params: {
                    "from": 0,
                    "limit": 100,
                    "so": {
                        message: form.findField('message').getSubmitValue(),
                        fromDate: form.findField('fromDate').getSubmitValue(),
                        toDate: form.findField('toDate').getSubmitValue()
                    }
                },
                success: this.onResultCdrReportSuccess,
                scope: this,
                failure: function (form, action) {
                    Ext.Msg.alert('Failed', action.result.msg);
                }
            });
        }
    },
    onResultCdrReportSuccess: function (response) {
        var store = this.getStore(),
            responseJSON = response.responseJSON,
            responseRecords;
        if (responseJSON.success) {
            responseRecords = responseJSON.records;
            //do so
            var grid = this.up('cdrtype').down('grid');
            grid.getStore().load();
        }
    }
});

