/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.controller.CdrTypeController', {
    extend: 'Ext.app.Controller',

    stores: ['CdrTypeStore'],

    models: ['CdrModel'],

    views: ['CdrType'],

    refs: [{
        ref: 'cdrtype',
        selector: 'cdrtype'
    }],

    id: 'cdrtypecontroller',

    init: function () {
        this.control({
            'cdrtype button[itemId=searchtype]': {
                click: this.doCdrTypeReportJob
            }
        });
    },

    doCdrTypeReportJob: function (button) {
        "use strict";
        var form = button.up().up('panel').down('form'),
            values = form.getValues(),
            so = values && values.type && values.type.join(',');
        if (so) {
            Ext.StoreManager.lookup('cdrTypeStore').load({
                params: {cdrType: so}
            });
        }
    }
});

