/**
 * Created by Samira on 8/13/2017.
 */

Ext.define('cdrReport.view.Cdr', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.cdr',
    store: 'CdrStore',

    requires: [
        'Ext.grid.GridPanel',
        'Ext.button.Button',
        'Ext.form.TextField',
        'cdrReport.store.CdrStore'
    ],


    id: 'cdr',

    initComponent : function () {
        this.items = [
            /*{
                xtype:'panel',
                items: [*/
                    {
                        xtype:'form',
                        width: 500,
                        height: 200,
                        floating: false,
                        margin: '10 5 3 10',
                        items : [
                            {
                                xtype: 'textfield',
                                name: 'fromDate',
                                text: 'from Date',
                                fieldLabel: 'from Date',
                                width: 300,
                                margin: '10 5 3 10',
                                allowBlank: true
                            }, {
                                xtype: 'textfield',
                                name: 'toDate',
                                fieldLabel: 'to Date',
                                width: 300,
                                margin: '10 5 3 10',
                                allowBlank: true
                            },
                            {
                                xtype: 'textfield',
                                name: 'message',
                                fieldLabel: 'message',
                                width: 300,
                                margin: '10 5 3 10',
                                allowBlank: true
                            },
                            {
                                xtype:'button',
                                text: 'Search',
                                width: 100,
                                height: 30,
                                margin: '10 5 3 120',
                                itemId: 'search',
                                handler : this.doCdrReportJob,
                                scope: this
                            }
                        ]
                    }
/*
                ]
            }*/,
            {
                xtype: 'panel',
                tile: 'test',
                items: [
                    {
                        xtype: 'grid',
                        title: 'CDR TYPE RESULT',
                        store: {
                            type: 'cdrs'
                        },
                        columns: [{
                            text: "id",
                            width: 170,
                            flex: 1,
                            dataIndex: 'id',
                            sortable: true
                        }, {
                            text: "time",
                            width: 160,
                            flex: 1,
                            dataIndex: 'time',
                            sortable: true
                        }, {
                            text: "sender",
                            width: 170,
                            flex: 1,
                            dataIndex: 'sender',
                            sortable: true
                        }, {
                            text: "receiver",
                            width: 160,
                            flex: 1,
                            dataIndex: 'receiver',
                            sortable: true
                        }, {
                            text: "cdrType",
                            width: 160,
                            flex: 1,
                            dataIndex: 'cdrType',
                            sortable: true
                        }, {
                            text: "message",
                            width: 160,
                            flex: 1,
                            dataIndex: 'message',
                            sortable: true
                        }]
                    }
                ]
            }

        ];

        this.callParent();
    }

});
