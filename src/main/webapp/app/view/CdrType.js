/**
 * Created by Samira on 8/13/17.
 */

Ext.define('cdrReport.view.CdrType', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.cdrtype',

    store: 'CdrTypeStore',

    requires: [
        'Ext.grid.GridPanel',
        'Ext.button.Button',
        'Ext.form.TextField',
        'cdrReport.store.CdrTypeStore'
    ],


    id: 'cdrtype',

    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                border: true,
                height: 180,
                margin: '10 5 3 10',
                items: [
                    {
                        xtype: 'checkboxgroup',
                        allowBlank: true,
                        columns: 1,
                        itemId: 'cdrTypeGroup',
                        labelWidth: 70,
                        items: [
                            {
                                boxLabel: 'ATTACH',
                                name: 'type',
                                inputValue: 'ATTACH',
                                stateEvents : ['change'],
                                stateId: 'ATTACH',
                                stateful: true
                            },
                            {
                                boxLabel: 'DETACH',
                                name: 'type',
                                inputValue:'DETACH',
                                stateEvents : ['change'],
                                stateId: 'DETACH',
                                stateful: true
                            },
                            {
                                boxLabel: 'REGISTER',
                                name: 'type',
                                inputValue:'REGISTER',
                                stateEvents : ['change'],
                                stateId: 'REGISTER',
                                stateful: true
                            },
                            {
                                boxLabel: 'ENTER',
                                name: 'type',
                                inputValue:'ENTER',
                                stateEvents : ['change'],
                                stateId: 'ENTER',
                                stateful: true
                            },
                            {
                                boxLabel: 'EXIT',
                                name: 'type',
                                inputValue:'EXIT',
                                stateEvents : ['change'],
                                stateId: 'EXIT',
                                stateful: true
                            }
                        ]
                    },
                    {
                        xtype: 'button',
                        text: 'Search',
                        width: 100,
                        height: 30,
                        margin: '10 5 3 120',
                        itemId: 'searchtype',
                        handler: this.doCdrTypeReportJob,
                        scope: this
                        /*  handler: function () {
                         var grid = this.up('cdrtype').down('grid');
                         grid.getStore().load();
                         }*/
                    }
                ]
            },
            {
                xtype: 'panel',
                tile: 'test',
                items: [
                    {
                        xtype: 'grid',
                        title: 'CDR TYPE RESULT',
                        store: {
                            type: 'cdrtypes'
                        },
                        columns: [
                            {
                                text: "cdrType",
                                width: 170,
                                flex: 1,
                                dataIndex: 'cdrType',
                                sortable: true
                            },
                            {
                                text: "count",
                                width: 160,
                                flex: 1,
                                dataIndex: 'count',
                                sortable: true
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent();
    }

});
