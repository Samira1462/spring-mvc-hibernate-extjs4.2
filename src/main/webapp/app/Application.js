Ext.define('cdrReport.Application', {
    name: 'cdrReport',

    extend: 'Ext.app.Application',
    rtl: true,
    views: [
        // add views here
        'Cdr',
        'CdrType'
    ],

    controllers: [
        // add controllers here
        'CdrTypeController',
        'CdrController'
    ],

    stores: [
        // add stores here
        'CdrTypeStore',
        'CdrStore'
    ]
});
