/**
 * Created by Samira on 8/13/17.
 */
Ext.define('cdrReport.store.CdrTypeStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cdrtypes',
    requires: 'cdrReport.model.CdrTypeModel',
    model: 'cdrReport.model.CdrTypeModel',

    storeId:'cdrTypeStore',

    proxy: {
        type: 'ajax',
        url: 'cdr/report/type/list',
        headers: {'Content-Type' : 'application/json' },
        reader: {
            type: 'json',
            root: 'resultCode'
        }
    }
});