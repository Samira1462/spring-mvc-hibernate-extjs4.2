/**
 * Created by Samira on 8/12/17.
 */
Ext.define('cdrReport.store.CdrStore', {
    extend: 'Ext.data.Store',
    alias: 'store.cdrs',
    requires: 'cdrReport.model.CdrModel',
    model: 'cdrReport.model.CdrModel',

    storeId:'cdrStore',

    proxy: {
        type: 'ajax',
        url: 'http://127.0.0.1:8080/cdr/report/list',
        reader: {
            type: 'json',
            root: 'resultCode'
        }
    }
});