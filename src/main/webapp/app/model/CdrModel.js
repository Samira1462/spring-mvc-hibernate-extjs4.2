/**
 * Created by Samira on 8/12/17.
 */
Ext.define('cdrReport.model.CdrModel', {
    extend: 'Ext.data.Model',
    fields: ['id', 'time', 'sender', 'receiver', 'cdrType', 'message']

   /* proxy: {
        type: 'ajax',
        url: 'data/recentsongs.json',
        reader: {
            type: 'json',
            root: 'results'
        }
    }*/
});